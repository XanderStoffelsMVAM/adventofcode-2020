﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            var hills = File.ReadAllLines("input/input.txt");
            var hillHight = hills.Length;
            var hillWidth = hills[0].Length;
            const char hit = '#';
           
            long CountTreesHit((int x, int y) slope)
            {
                var hits = 0L;
                var toboggan = new Toboggan(0, 0);
                while (toboggan.PosY < hillHight - 1)
                {
                    // Tried to use modulo first, but is slower.
                    toboggan += slope;
                    if (toboggan.PosX >= hillWidth)
                        toboggan = new Toboggan(0, toboggan.PosY);
                    if (hills[toboggan.PosY][toboggan.PosX] == hit)
                        hits++;
                }
                return hits;
            }

            var watch = Stopwatch.StartNew();
            var result = new List<(int x, int y)> { (1, 1), (3, 1), (5, 1), (7, 1), (1, 2) }
                .Select(CountTreesHit)
                 .Aggregate(1L, (x, y) => x * y);
            watch.Stop();
            Console.WriteLine(result);
            Console.WriteLine($"{watch.ElapsedMilliseconds} ms");
        }

        public record Toboggan(int PosX, int PosY)
        {
            public static Toboggan operator +(Toboggan toboggan, (int x, int y) slope)
                => new Toboggan(toboggan.PosX + slope.x, toboggan.PosY + slope.y);
        }
    }
}