﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines("input/input.txt");

            var watch = Stopwatch.StartNew();
            var validPasses = 0;

            // The boring way.
            /*
            foreach(var line in input)
            {
                if (ValidatePassword(Parse(line)))
                    validPasses++;
            }
            */
        
            // The C# way.
            validPasses = input
                .Select(Parse)
                .Select(ValidatePassword)
                .Count(b => b);
            
            watch.Stop();

            Console.WriteLine(validPasses);
            Console.WriteLine($"Took {watch.ElapsedMilliseconds} ms");
        }

        static (int min, int max, char c, string pass) Parse(string line)
        {
            var parts = line.Split(" ");
            var minmax = parts[0].Split("-");
            return (int.Parse(minmax[0]), int.Parse(minmax[1]), parts[1][0], parts[2]);
        }

        static bool ValidatePassword((int pos1, int pos2, char c, string pass) v)
        {
            if (v.pass[v.pos1 - 1] == v.c && v.pass[v.pos2 - 1] != v.c) return true;
            if (v.pass[v.pos2 - 1] == v.c && v.pass[v.pos1 - 1] != v.c) return true;
            return false;
        }

        static bool OldValidatePassword((int min, int max, char c, string pass) v)
        {
            var count = v.pass.Count(pc => pc == v.c);
            return count >= v.min && count <= v.max;
        }   
    }
}
