﻿using System;
using System.Reflection;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace AdventOfCode
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var challenges = Assembly.GetExecutingAssembly()
                 .GetTypes()
                 .Where(t => t.IsClass && t.IsAssignableTo(typeof(IChallenge)))
                 .Select(t => {
                     Console.WriteLine($"Found challenge: {t.Name}");
                     return (IChallenge)Activator.CreateInstance(t);
                 })
                 .ToList();

            var loading = challenges.Select(c => c.LoadDataAsync());
            Console.WriteLine("Loading input files...");
            await Task.WhenAll(loading);

            Console.WriteLine("Running tasks...");
            foreach (var result in RunSync(challenges))
                Console.WriteLine($"{result.name, -8}: '{result.result, -20}' in {result.time, 5} ms");
        }

        public static IEnumerable<ChallangeResult> RunSync(List<IChallenge> challenges)
        {
            foreach (var challange in challenges)
            {
                var watch = Stopwatch.StartNew();
                var result = challange.Run();
                watch.Stop();
                yield return new ChallangeResult(challange.GetType().Name, watch.ElapsedMilliseconds, result);
            }
        }

        public static IEnumerable<ChallangeResult> RunParallell(List<IChallenge> challenges)
        {
            var bag = new ConcurrentBag<ChallangeResult>();
            Parallel.ForEach(challenges, challange =>
            {
                var watch = Stopwatch.StartNew();
                var result = challange.Run();
                watch.Stop();
                bag.Add(new ChallangeResult(challange.GetType().Name, watch.ElapsedMilliseconds, result));
            });
            return bag;
        }

        public record ChallangeResult(string name, long time, string result);
    }
}
