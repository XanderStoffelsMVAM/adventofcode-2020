﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Challenges
{
    public class Day4 : IChallenge
    {
        public string[] Data { get; set; }
        private static readonly string[] _requiredProps = new[] { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };
        public async Task LoadDataAsync()
        {
            Data = (await File.ReadAllTextAsync("Input/input4.txt")).Split("\n\n");
        }

        public string Run()
        {
            return Data
                .Select(seq => seq.Replace("\n", " ").Split(" "))
                .Select(PropsToDict)
                .Select(ValidatePassport)
                .Count(v => v)
                .ToString();
        }

        public static Dictionary<string, string> PropsToDict(string[] stringProps)
            => stringProps
                .Where(s => s.Split(":").Length == 2)
                .ToDictionary(p => p.Split(":")[0], p => p.Split(":")[1]);


        public static bool ValidatePassport(Dictionary<string, string> props)
        => _requiredProps
                .Select(rprop => props.TryGetValue(rprop, out var val) && ValidateProp(rprop, val))
                .All(v => v);
        
        private static bool ValidateProp(string rprop, string val)
        {
            if (string.IsNullOrWhiteSpace(val)) return false;

            const string hexChars = "0123456789abcdef";
            string[] eyeColors = new[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" };
            static bool Between(int v, int min, int max)
                => v >= min && v <= max; 

            var r = rprop switch
            {
                "byr" => val.Length == 4 && int.TryParse(val, out var intVar) && Between(intVar, 1920, 2002),
                "iyr" => val.Length == 4 && int.TryParse(val, out var intVar) && Between(intVar, 2010, 2020),
                "eyr" => val.Length == 4 && int.TryParse(val, out var intVar) && Between(intVar, 2020, 2030),
                "hgt" => val.Length >= 3 && val.Substring(val.Length - 2, 2) switch
                {
                    "cm" => val.Length > 2 && int.TryParse(val.Substring(0, val.Length - 2), out var intVal) && Between(intVal, 150, 193),
                    "in" => val.Length > 1 && int.TryParse(val.Substring(0, val.Length - 2), out var intVal) && Between(intVal, 59, 76),
                    _ => false
                },
                "hcl" => val.Length == 7 && val.StartsWith("#") && val.Skip(1).All(c => hexChars.Contains(c)),
                "ecl" => val.Length == 3 && eyeColors.Contains(val),
                "pid" => val.Length == 9 && val.All(n => int.TryParse($"{n}", out var _)),
                "cid" => true,
                _ => throw new NotImplementedException(),
            };
            return r;
        }
    }
}
