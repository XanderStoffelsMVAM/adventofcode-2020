﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Challenges
{
    public class Day3 : IChallenge
    {
        public string[] Data { get; set; }
        public async Task LoadDataAsync()
        {
            Data = await File.ReadAllLinesAsync("Input/input3.txt");
        }

        public string Run()
        {
            var hillHight = Data.Length;
            var hillWidth = Data[0].Length;
            const char hit = '#';

            long CountTreesHit((int x, int y) slope)
            {
                var hits = 0L;
                var toboggan = new Toboggan(0, 0);
                while (toboggan.PosY < hillHight - 1)
                {
                    // Tried to use modulo first, but is slower.
                    toboggan += slope;
                    if (toboggan.PosX >= hillWidth)
                        toboggan = new Toboggan(0, toboggan.PosY);
                    if (Data[toboggan.PosY][toboggan.PosX] == hit)
                        hits++;
                }
                return hits;
            }

            return new List<(int x, int y)> { (1, 1), (3, 1), (5, 1), (7, 1), (1, 2) }
                    .Select(CountTreesHit)
                    .Aggregate(1L, (x, y) => x * y)
                    .ToString();       
        }

        public record Toboggan(int PosX, int PosY)
        {
            public static Toboggan operator +(Toboggan toboggan, (int x, int y) slope)
                => new Toboggan(toboggan.PosX + slope.x, toboggan.PosY + slope.y);
        }
    }
}
