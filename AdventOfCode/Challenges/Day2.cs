﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Challenges
{
    public class Day2 : IChallenge
    {
        public string[] Data { get; set; }
        public async Task LoadDataAsync()
        {
            Data = await File.ReadAllLinesAsync("Input/input2.txt");
        }

        public string Run()       
            => Data
                .Select(Parse)
                .Select(ValidatePassword)
                .Count(b => b)
                .ToString();     

        static (int min, int max, char c, string pass) Parse(string line)
        {
            var parts = line.Split(" ");
            var minmax = parts[0].Split("-");
            return (int.Parse(minmax[0]), int.Parse(minmax[1]), parts[1][0], parts[2]);
        }
        static bool ValidatePassword((int pos1, int pos2, char c, string pass) v)  
            => (v.pass[v.pos1 - 1] == v.c && v.pass[v.pos2 - 1] != v.c) ||
               (v.pass[v.pos2 - 1] == v.c && v.pass[v.pos1 - 1] != v.c);
        
    }
}
