﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Challenges
{
    public class Day1 : IChallenge
    {
        public int[] Data { get; set; }
        public async Task LoadDataAsync()
        {
            var lines = await File.ReadAllLinesAsync("Input/input1.txt");
            Data = lines.Select(i => int.Parse(i)).ToArray();
        }

        public string Run()
        {
            for (var i = 0; i < Data.Length; i++)
            {
                if (Data[i] >= 2020) continue;
                for (var j = i + 1; j < Data.Length; j++)
                {
                    var ij = Data[i] + Data[j];
                    if (ij >= 2020) continue;
                    for (var k = j + 1; k < Data.Length; k++)  
                        if (ij + Data[k] == 2020)
                           return (Data[i] * Data[j] * Data[k]).ToString();
                }
            }
            throw new ChallengeException("No solution found.");
        }
    }
}
