﻿using System;
using System.IO;
using System.Linq;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines("input/input.txt")
                .Select(i => int.Parse(i))
                .ToArray();


            for (var i = 0; i < input.Length; i++)
            {
                if (input[i] >= 2020) continue;
                for (var j = i + 1; j < input.Length; j++)
                {
                    var ij = input[i] + input[j];
                    if (ij >= 2020) continue;              
                    for (var k = j + 1; k < input.Length; k++)
                    {
                        if (ij + input[k] == 2020)
                        {
                            Console.WriteLine(input[i] * input[j] * input[k]);
                            return;
                        }
                    }
                }
            } 
            Console.WriteLine("No solution");
        }
    }
}
